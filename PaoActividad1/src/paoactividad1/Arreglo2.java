
package paoactividad1;

public class Arreglo2 {
    
    static String[] nombres = {"Erika","Raul","Diana","Luis","Aridai"};
    static int[]    edades  = {21, 23, 22, 24, 20};
    
    public static void fori_fore(){
        int[] nums  = {1,2,3,4,5,6,7,8,9,0};
        int[] bnums = (int[]) nums.clone() ;
        
        System.out.println("FOR i");
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            System.out.println(num);
            
        }

        System.out.println("For each");
        for (int num : nums) {
            System.out.println(num);
        }

        System.out.println("Clone");
        for (int bnum : bnums) {
            System.out.println(bnum);
        }
        
       int[] sarraycopy;
        sarraycopy = new int[5];

        System.arraycopy(bnums, 5, sarraycopy, 0,3);
        for (int i : sarraycopy) {
            System.out.println(i);
        }
       
    }

     public static void leernombres(){
         System.out.println("Leer Alumnos");
         for (int i = 0; i < nombres.length; i++) {
             System.out.print(nombres[i]+ "  ");
             System.out.println(edades[i]);
         }
     }

     public static void editAlumnos(){
         nombres[0] = "Matty";
         nombres[2] = "Pipe";
       nombres[4] = "Gaby";
         System.out.println("Modificar Alumnos");
         for (int i = 0; i < nombres.length; i++) {
             System.out.print(nombres[i]+ "  ");
             System.out.println(edades[i]);
         }
         
     }


    public static void main(String[] args) {
      leernombres();
        editAlumnos();
        
    }
}
